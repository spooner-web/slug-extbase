<?php

$EM_CONF[$_EXTKEY] = [
      'title' => 'Slug updater for Extbase',
      'description' => 'Utility to update slug fields after changing or creating Extbase objects in frontend',
      'version' => '2.0.0',
      'category' => 'misc',
      'state' => 'beta',
      'author' => 'Thomas Löffler',
      'author_email' => 'loeffler@spooner-web.de',
      'author_company' => 'Spooner Web',
      'clearCacheOnLoad' => true,
      'constraints' => [
            'depends' => [
              'typo3' => '10.4.0 - 11.5.99',
              'extbase' => '10.4.0 - 11.5.99',
            ],
            'conflicts' => [
            ],
            'suggests' => [
            ],
      ],
  /*
  'uploadfolder' => false,
  'createDirs' => NULL,
  'clearcacheonload' => false,
  */
];
