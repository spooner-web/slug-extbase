<?php
declare(strict_types=1);

namespace SpoonerWeb\SlugExtbase\EventListener;

/*
 * This file is part of a TYPO3 extension.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use SpoonerWeb\SlugExtbase\Services\SlugServices;
use TYPO3\CMS\Extbase\Event\Persistence\EntityAddedToPersistenceEvent;

class CreateSlug extends SlugServices
{
    /**
     * invoke
     *
     * @param EntityAddedToPersistenceEvent $event
     */
    public function __invoke(EntityAddedToPersistenceEvent $event): void
    {
        $this->updateSlugForObject($event->getObject());
    }
}
