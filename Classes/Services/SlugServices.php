<?php
declare(strict_types=1);

/*
 * This file is part of a TYPO3 extension.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace SpoonerWeb\SlugExtbase\Services;

use SpoonerWeb\SlugExtbase\SlugEntityInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Core\DataHandling\Model\RecordStateFactory;
use TYPO3\CMS\Core\DataHandling\SlugHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\DomainObject\DomainObjectInterface;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;

class SlugServices
{
    /**
     * @param DomainObjectInterface $object
     */
    public function updateSlugForObject(DomainObjectInterface $object)
    {
        // object must implement SlugEntityInterface
        if ($object instanceof SlugEntityInterface) {
            [$tableName, $slugFieldName] = $this->getTableAndSlugFieldName($object);
            // did we get a valid table name and slug-field?
            if ($tableName && $slugFieldName) {
                // find the desired field names to build the slug
                $fieldsToWatch = $GLOBALS['TCA'][$tableName]['columns'][$slugFieldName]['config']['generatorOptions']['fields'];
                foreach ($fieldsToWatch as $fieldToWatch) {
                    $fieldNameInObject = GeneralUtility::underscoredToLowerCamelCase($fieldToWatch);
                    if ($object->_isDirty($fieldNameInObject)) {
                        $this->generateAndSaveSlug($object->getUid(), $tableName, $slugFieldName);
                    }
                }
            }
        }
    }

    /**
     * @param DomainObjectInterface $object
     * @return array
     */
    protected function getTableAndSlugFieldName(DomainObjectInterface $object): array
    {
        $tableName = null;
        $slugFieldName = null;

        // get the table name
        $tableName = GeneralUtility::makeInstance(DataMapper::class)->convertClassNameToTableName(get_class($object));

        // find the slug field
        foreach ($GLOBALS['TCA'][$tableName]['columns'] as $field => $config) {
            // convert to lower for correct string comparison
            if (strtolower($config['config']['type']) === 'slug') {
                $slugFieldName = $field;
                break;
            }
        }

        return [$tableName, $slugFieldName];
    }

    /**
     * @param int $objectUid
     * @param string $tableName
     * @param string $slugFieldName
     */
    protected function generateAndSaveSlug(int $objectUid, string $tableName, string $slugFieldName)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($tableName);
	    $queryBuilder->getRestrictions()->removeByType(HiddenRestriction::class);
        $record = $queryBuilder
            ->select('*')
            ->from($tableName)
            ->where($queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($objectUid)))
            ->execute()
            ->fetch();

        $helper = GeneralUtility::makeInstance(
            SlugHelper::class,
            $tableName,
            $slugFieldName,
            $GLOBALS['TCA'][$tableName]['columns'][$slugFieldName]['config']
        );

        $value = $helper->generate($record, $record['pid']);
        $state = RecordStateFactory::forName($tableName)->fromArray($record, $record['pid'], $record['uid']);

        if (GeneralUtility::inList($GLOBALS['TCA'][$tableName]['columns'][$slugFieldName]['config']['eval'], 'uniqueInPid')) {
            $value = $helper->buildSlugForUniqueInPid($value, $state);
        } else {
            $value = $helper->buildSlugForUniqueInSite($value, $state);
        }

        $queryBuilder
            ->update($tableName)
            ->where(
                $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($objectUid))
            )
            ->set($slugFieldName, $value)
            ->execute();
    }
}
